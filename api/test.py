from fastapi import FastAPI
from pydantic import BaseModel
from pydantic.types import Json

token = "7caL5il5U7qFCupPc6IJFrm6UJ0i9rEL7LkL0q0VjF3c_ObWR_yzRtV936Ab3VvF_B1wqbS0AQW8Y4G4CMQgJg=="

app = FastAPI()

class climate(BaseModel):
    key: str = None
    node : str
    data : list

@app.post("/climate")
async def read_user(request: climate):
    if (request.key == token):
        node = request.node
        data = request.data
        datas = []
        for i in data:
            data = "data,host=" + node + " " + i["name"] + "=" + str(i["value"])
            datas.append(data)

        return {"status":"success","node":datas}
    else:
        return {"status":"Failure"}