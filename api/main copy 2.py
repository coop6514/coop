from fastapi import FastAPI
from pydantic import BaseModel
from influxdb_client import InfluxDBClient
from influxdb_client.client.write_api import SYNCHRONOUS

token = "WFF5gaQzok9FYX1mFtp4PCUP2ogg6xhaH32cJiaH5YfY4EiwZlz_ITqdVtMT7xq3ECgaUiVLdydaeRUGoOXU_w=="
org = "nitiphoom"
bucket = "climate"

client =  InfluxDBClient(url="http://172.16.10.48:8086/", token=token, org=org)

app = FastAPI()

class climate(BaseModel):
   node : str
   data : list

@app.post("/climate")
async def read_user(request: climate):
    write_api = client.write_api(write_options=SYNCHRONOUS)
    node = request.node
    data = request.data
    datas = []
    for i in data:
        data = "data,host=" + node + " " + i["name"] + "=" + str(i["value"])
        write_api.write(bucket, org, data)
        datas.append(data)

    return {"status":"success","data":datas}