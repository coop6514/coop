from fastapi import FastAPI
from pydantic import BaseModel
from influxdb_client import InfluxDBClient
from influxdb_client.client.write_api import SYNCHRONOUS

token = "7caL5il5U7qFCupPc6IJFrm6UJ0i9rEL7LkL0q0VjF3c_ObWR_yzRtV936Ab3VvF_B1wqbS0AQW8Y4G4CMQgJg=="
org = "dev-team"
bucket = "climate"

client =  InfluxDBClient(url="http://172.16.20.252:8086/", token=token, org=org)

app = FastAPI()

class climate(BaseModel):
    key: str = None
    node : str
    data : list

@app.post("/climate")
async def read_user(request: climate):
    if (request.key == token):
        write_api = client.write_api(write_options=SYNCHRONOUS)
        node = request.node
        data = request.data
        datas = []
        for i in data:
            data = "data,host=" + node + " " + i["name"] + "=" + str(i["value"])
            write_api.write(bucket, org, data)
            datas.append(data)

        return {"status":"success","data":datas}
    else:
        return {"status":"Failure"}