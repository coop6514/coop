#include <SPI.h>
#include <LoRa.h>

int counter = 0;

void setup() {
  Serial.begin(9600);
  //while (!Serial);

  Serial.println("LoRa Sender");
  //(ss, rst, DIO0)
  //LoRa.setPins(8, 4, 7);//LoRa32u4
  //LoRa.setPins(10, 9, 2);//arduino uno
  LoRa.setPins(5, 17, 16);//esp32 lite
  //LoRa.setPins(D8, D1, D2);//nodemcu esp8266
  if (!LoRa.begin(433E6)) {
    Serial.println("Starting LoRa failed!");
    while (1);
  }
}

void loop() {
  Serial.print("Sending packet: ");
  Serial.println(counter);

  // send packet
  LoRa.beginPacket();
  LoRa.print("yo ");
  LoRa.print(counter);
  LoRa.endPacket();

  counter++;

  delay(1000);
}
