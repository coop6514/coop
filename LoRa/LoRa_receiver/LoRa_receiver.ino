#include <SPI.h>
#include <LoRa.h>

void setup() {
  Serial.begin(9600);
  while (!Serial);

  Serial.println("LoRa Receiver");
  //(ss, rst, DIO0)
  //LoRa.setPins(8, 4, 7);//LoRa32u4
  //LoRa.setPins(10, 9, 2);//arduino uno
  //LoRa.setPins(5, 17, 16);//esp32 lite
  LoRa.setPins(D8, D1, D2);//nodemcu esp8266
  if (!LoRa.begin(433E6)) {
    Serial.println("Starting LoRa failed!");
    while (1);
  }
  Serial.println("Starting LoRa Succes");
}

void loop() {
  // try to parse packet
  //Serial.print("*");
  int packetSize = LoRa.parsePacket();
  if (packetSize) {
    // received a packet
    Serial.println();
    Serial.print("Received packet '");

    // read packet
    while (LoRa.available()) {
      Serial.print((char)LoRa.read());
    }

    // print RSSI of packet
    Serial.print("' with RSSI ");
    Serial.println(LoRa.packetRssi());
  }
  delay(1);
}
