#include <SPI.h>              // include libraries
#include <LoRa.h>
#include "DHT.h"

#define DHTPIN 2

#define DHTTYPE DHT22

DHT dht(DHTPIN, DHTTYPE);

String outgoing;              // outgoing message

byte msgCount = 0;            // count of outgoing messages
byte MasterNode = 0xFF;
byte Node = 0xBB;
int count = 0;

String DHT_sensor() {
  String output = "[{\"name\":\"Temperature\", \"unit\":\"°C\", \"value\":none}, ";
  output += "{\"name\":\"Humidity\", \"unit\":\"%\", \"value\":none}, ";
  output += "{\"name\":\"FeelLike\", \"unit\":\"°C\", \"value\":none}]";
  float h = dht.readHumidity();
  float t = dht.readTemperature();

  if (isnan(h) || isnan(t)) {
    Serial.println(F("Failed to read from DHT sensor!"));
    return output;
  }
  else {
    float hic = dht.computeHeatIndex(t, h, false);
    output = "[{\"name\":\"Temperature\", \"unit\":\"°C\", \"value\":" + String(t) + "}, " +
             "{\"name\":\"Humidity\", \"unit\":\"%\", \"value\":" + String(h) + "}, " +
             "{\"name\":\"FeelLike\", \"unit\":\"°C\", \"value\":" + String(hic) + "}]";
    return output;
  }
}

void setup() {
  Serial.begin(9600);
  dht.begin();
  //while (!Serial);

  Serial.println("LoRa Duplex");

  //(ss, rst, DIO0)
  LoRa.setPins(8, 4, 7);//LoRa32u4
  //LoRa.setPins(10, 9, 2);//arduino uno
  //LoRa.setPins(5, 17, 16);//esp32 lite
  //LoRa.setPins(D8, D1, D2);//nodemcu esp8266
  if (!LoRa.begin(433E6)) {             // initialize ratio at 915 MHz
    Serial.println("LoRa init failed. Check your connections.");
    while (true);                       // if failed, do nothing
  }

  Serial.println("LoRa init succeeded.");
}

void loop() {
  // parse for a packet, and call onReceive with the result:
  onReceive(LoRa.parsePacket());
  Serial.print("*");
  /*if (count >= 10000) {
    ESP.restart();// for reset esp
  }*/
  delay(1);
}

void sendMessage(String outgoing, byte MasterNode, byte otherNode) {
  LoRa.beginPacket();                   // start packet
  LoRa.write(MasterNode);              // add destination address
  LoRa.write(Node);             // add sender address
  LoRa.write(msgCount);                 // add message ID
  LoRa.write(outgoing.length());        // add payload length
  LoRa.print(outgoing);                 // add payload
  LoRa.endPacket();                     // finish packet and send it
  msgCount++;                           // increment message ID
}

void onReceive(int packetSize) {
  if (packetSize == 0) return;          // if there's no packet, return

  // read packet header bytes:
  int recipient = LoRa.read();          // recipient address
  byte sender = LoRa.read();            // sender address
  byte incomingMsgId = LoRa.read();     // incoming msg ID
  byte incomingLength = LoRa.read();    // incoming msg length

  String incoming = "";

  if (incomingLength == incoming.length()) {   // check length for error
    if (recipient == Node && sender == MasterNode) {
      while (LoRa.available()) {
        incoming += (char)LoRa.read();
      }

      Serial.println();
      Serial.println(" incoming : " + String(incoming));
      int Val = incoming.toInt();
      if (Val == 34) {
        String message = DHT_sensor();
        Serial.println(message);
        sendMessage(message, MasterNode, Node);
      }

      count = 0;
    }
  }
}
