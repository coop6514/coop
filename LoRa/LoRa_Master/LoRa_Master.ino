#include <SPI.h>              // include libraries
#include <LoRa.h>

byte MasterNode = 0xFF;
byte Node1 = 0xBB;
byte Node2 = 0xCC;

String SenderNode = "";
String outgoing;              // outgoing message

byte msgCount = 0;            // count of outgoing messages

// Tracks the time since last event fired
unsigned long previousMillis = 0;
unsigned long int previoussecs = 0;
unsigned long int currentsecs = 0;
unsigned long currentMillis = 0;
int interval = 1 ; // updated every 1 second
int Secs = 0;


void setup() {
  Serial.begin(9600);                   // initialize serial
  //while (!Serial);
  //(ss, rst, DIO0)
  //LoRa.setPins(8, 4, 7);//LoRa32u4
  //LoRa.setPins(10, 9, 2);//arduino uno
  LoRa.setPins(5, 17, 16);//esp32 lite
  //LoRa.setPins(D8, D1, D2);//nodemcu esp8266
  if (!LoRa.begin(433E6)) {             // initialize ratio at 915 MHz
    Serial.println("LoRa init failed. Check your connections.");
    while (true);                       // if failed, do nothing
  }

  Serial.println("LoRa init succeeded.");
}

void loop() {

  currentMillis = millis();
  currentsecs = currentMillis / 2000;
  if ((unsigned long)(currentsecs - previoussecs) >= interval) {
    //Serial.println(Secs);
    if ( Secs == 1 ) {
      String message = "34";
      sendMessage(message, MasterNode, Node1);
      Secs = 0;
    }
    else {
      String message = "55";
      sendMessage(message, MasterNode, Node2);
      Secs = 1;
    }

    previoussecs = currentsecs;
  }

  // parse for a packet, and call onReceive with the result:
  onReceive(LoRa.parsePacket());
  delay(100);
}

void sendMessage(String outgoing, byte MasterNode, byte otherNode) {
  LoRa.beginPacket();                   // start packet
  LoRa.write(otherNode);              // add destination address
  LoRa.write(MasterNode);             // add sender address
  LoRa.write(msgCount);                 // add message ID
  LoRa.write(outgoing.length());        // add payload length
  LoRa.print(outgoing);                 // add payload
  LoRa.endPacket();                     // finish packet and send it
  msgCount++;                           // increment message ID
}

void onReceive(int packetSize) {
  if (packetSize == 0) return;          // if there's no packet, return

  // read packet header bytes:
  int recipient = LoRa.read();          // recipient address
  byte sender = LoRa.read();            // sender address
  if ( sender == 0XBB )
    SenderNode = "Node1";
  if ( sender == 0XCC )
    SenderNode = "Node2";

  if (SenderNode != "") {
    byte incomingMsgId = LoRa.read();     // incoming msg ID
    byte incomingLength = LoRa.read();    // incoming msg length

    String incoming = "";

    while (LoRa.available()) {
      incoming += (char)LoRa.read();
    }

    if (incomingLength != incoming.length()) {   // check length for error
      //Serial.println("error: message length does not match length");
      ;
      return;                             // skip rest of function
    }

    // if the recipient isn't this device or broadcast,
    if (recipient != Node1 && recipient != MasterNode) {
      // Serial.println("This message is not for me.");
      ;
      return;                             // skip rest of function
    }

    Serial.println(SenderNode + " : " + incoming);
    SenderNode = "";
  }


}
