#include <SPI.h>              // include libraries
#include <LoRa.h>

String outgoing;              // outgoing message

byte msgCount = 0;            // count of outgoing messages
byte MasterNode = 0xFF;
byte Node = 0xCC;
int reset_count = 0;

String WindSpeed() {
  int raw = analogRead(A0);
  float volt = raw * (5.0 / 1023.0);
  int WindSpeed = volt * 6;
  return "[{\"name\":\"WindSpeed\", \"unit\":\"m/s\", \"value\":" + String(WindSpeed) + "}]";
}

void setup() {
  Serial.begin(9600);
  //while (!Serial);
  delay(1000);
  Serial.println("LoRa Duplex");

  //(ss, rst, DIO0)
  //LoRa.setPins(8, 4, 7);//LoRa32u4
  //LoRa.setPins(10, 9, 2);//arduino uno
  //LoRa.setPins(5, 17, 16);//esp32 lite
  LoRa.setPins(D8, D1, D2);//nodemcu esp8266
  if (!LoRa.begin(433E6)) {             // initialize ratio at 915 MHz
    Serial.println("LoRa init failed. Check your connections.");
    while (true);                       // if failed, do nothing
  }

  Serial.println("LoRa init succeeded.");
}

void loop() {
  // parse for a packet, and call onReceive with the result:
  if(reset_count > 10000){
    ESP.restart(); 
  }
  else{
    reset_count += 1;
  }
  onReceive(LoRa.parsePacket());
  //Serial.print("*");
  delay(1);
}

void sendMessage(String outgoing, byte MasterNode, byte otherNode) {
  LoRa.beginPacket();                   // start packet
  LoRa.write(MasterNode);              // add destination address
  LoRa.write(Node);             // add sender address
  LoRa.write(msgCount);                 // add message ID
  LoRa.write(outgoing.length());        // add payload length
  LoRa.print(outgoing);                 // add payload
  LoRa.endPacket();                     // finish packet and send it
  msgCount++;                           // increment message ID
}

void onReceive(int packetSize) {
  if (packetSize == 0) return;          // if there's no packet, return

  // read packet header bytes:
  int recipient = LoRa.read();          // recipient address
  byte sender = LoRa.read();            // sender address
  byte incomingMsgId = LoRa.read();     // incoming msg ID
  byte incomingLength = LoRa.read();    // incoming msg length
  
  if (recipient == Node && sender == MasterNode) {
    int count = 0;
    String incoming = "";
    while (LoRa.available()) {
      incoming += (char)LoRa.read();
      if (count > 100) {
        break;
      } else {
        count++;
      }
    }
    if (incomingLength == incoming.length()) {
      Serial.println();
      Serial.println(" incoming : " + String(incoming));
      int Val = incoming.toInt();
      if (Val == 55) {
        String message = WindSpeed();
        Serial.println(message);
        sendMessage(message, MasterNode, Node);
      }
    }
  }
}
