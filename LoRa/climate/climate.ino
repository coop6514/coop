#include "DHT.h"

#define DHTPIN 4
//#define DHTTYPE DHT11   // DHT 11
#define DHTTYPE DHT22   // DHT 22  (AM2302), AM2321
//#define DHTTYPE DHT21   // DHT 21 (AM2301)

DHT dht(DHTPIN, DHTTYPE);

int velocity(){
  int sensorValue = analogRead(A0);
  float outvoltage = sensorValue * (5.0 / 1023.0);
  int Level = 6 * outvoltage; //The level of wind speed is proportional to the output voltage.
  return Level;
}

String climate(){
  //Serial.println("Reading Value");
  float humidity = dht.readHumidity();
  float temperature = dht.readTemperature();
  if (isnan(humidity) || isnan(temperature)) {
    Serial.println(F("Failed to read from DHT sensor!"));
    return;
  }
  float feel_like = dht.computeHeatIndex(temperature, humidity, false);

  String output = "{\"humidity\":" + String(humidity) +
                  ",\"temperature\":" + String(temperature) +
                  ",\"feel_like\":" + String(feel_like) + 
                  ",\"wind_speed\":" + String(velocity()) +"}";

  return output;
}

void setup() {
  Serial.begin(9600);
  //Serial.println(F("DHTxx test!"));

  dht.begin();
}

void loop() {
  delay(2000);
  Serial.println(climate());
}
