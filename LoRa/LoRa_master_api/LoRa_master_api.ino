#include <SPI.h>              // include libraries
#include <LoRa.h>

#include <HTTPClient.h>
#include "WiFi.h"

const char* ssid = "LOOK! WIFI";
const char* password =  "Look123!";

const String endpoint = "http://172.16.10.50:8000/climate";

byte MasterNode = 0xFF;
byte Node[] = {0xBB, 0xCC};
String outsending[] = {"34", "55"};

String SenderNode = "";
String outgoing;              // outgoing message

byte msgCount = 0;            // count of outgoing messages

// Tracks the time since last event fired
unsigned long previousMillis = 0;
unsigned long int previoussecs = 0;
unsigned long int currentsecs = 0;
unsigned long currentMillis = 0;
int interval = 1 ; // updated every 1 second
int Secs = 0;


boolean find(byte child, byte parent[]) {
  for (int i = 0; i < sizeof(parent) / sizeof(parent[0]); i++) {
    if (parent[i] == child) {
      return true;
    }
  }
  return false;
}

void setup() {
  Serial.begin(9600);                   // initialize serial
  //while(!Serial);
  //(int ss, int reset, int dio0)
  Serial.println("LoRa master");
  //LoRa.setPins(8, 4, 7);//LoRa32u4
  LoRa.setPins(5, 17, 16);//esp32 lite
  //LoRa.setPins(10, 9, 2);//arduino uno
  if (!LoRa.begin(433E6)) {             // initialize ratio at 915 MHz
    Serial.println("LoRa init failed. Check your connections.");
    while (true);                       // if failed, do nothing
  }

  Serial.println("LoRa init succeeded.");

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Connecting to WiFi..");
  }
  Serial.println("Connected to the WiFi network");
}

void loop() {

  currentMillis = millis();
  currentsecs = currentMillis / 1000;
  if ((unsigned long)(currentsecs - previoussecs) >= interval) {

    if (Secs + 1 < sizeof(Node) / sizeof(Node[0])) {
      Secs += 1;
    }
    else {
      Secs = 0;
    }

    String message = outsending[Secs];
    sendMessage(message, MasterNode, Node[Secs]);

    previoussecs = currentsecs;
  }

  // parse for a packet, and call onReceive with the result:
  onReceive(LoRa.parsePacket());
  //Serial.print("*");
  delay(1);
}

boolean send_data(String node, String json_data) {
  if ((WiFi.status() == WL_CONNECTED)) { //Check the current connection status

    HTTPClient http;

    http.begin(endpoint); //Specify the URL
    String json = "{\"key\":\"7caL5il5U7qFCupPc6IJFrm6UJ0i9rEL7LkL0q0VjF3c_ObWR_yzRtV936Ab3VvF_B1wqbS0AQW8Y4G4CMQgJg==\", \"node\":\"" + node + "\",\"data\":" + json_data + "}";
    int httpCode = http.POST(json);  //Make the request
    Serial.println("send data : \n" + json);
    if (httpCode >= 200 && httpCode < 300) { //Check for the returning code
      String payload = http.getString();
      Serial.print("httpCode : ");
      Serial.println(httpCode);
      Serial.print("output : ");
      Serial.println(payload);
      Serial.println();
      http.end();
      return true;
    }
    else {
      http.end();
      return false;
    }
  }
}

void sendMessage(String outgoing, byte MasterNode, byte otherNode) {
  Serial.println();
  Serial.println("send to : " + String(otherNode));
  LoRa.beginPacket();                   // start packet
  LoRa.write(otherNode);              // add destination address
  LoRa.write(MasterNode);             // add sender address
  LoRa.write(msgCount);                 // add message ID
  LoRa.write(outgoing.length());        // add payload length
  LoRa.print(outgoing);                 // add payload
  LoRa.endPacket();                     // finish packet and send it
  msgCount++;                           // increment message ID
}

void onReceive(int packetSize) {
  if (packetSize == 0) return;          // if there's no packet, return

  // read packet header bytes:
  int recipient = LoRa.read();          // recipient address MasterNode
  byte sender = LoRa.read();            // sender address
  if ( sender == 0XBB )
    SenderNode = "Node1";
  if ( sender == 0XCC )
    SenderNode = "Node2";

  if ( SenderNode != "") {

    byte incomingMsgId = LoRa.read();     // incoming msg ID
    byte incomingLength = LoRa.read();    // incoming msg length
    //Serial.println(String(recipient) + " " + String(find(recipient, Node)) + " " + String(sender == MasterNode));
    if (find(sender, Node) && recipient == MasterNode) {
      int count = 0;
      String incoming = "";
      while (LoRa.available()) {
        incoming += (char)LoRa.read();
        if (count > 200) {
          break;
        } else {
          count++;
        }
      }
      //Serial.println(String(incoming) + " " + String(incomingLength) + " " + String(incoming.length()) + " ");
      if (incomingLength == incoming.length()) {   // check length for error
        Serial.print(SenderNode);
        Serial.print(" : ");
        Serial.println(incoming);

        send_data(SenderNode, incoming);
      }
    }
    SenderNode = "";
  }
}
