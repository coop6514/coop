#include <SPI.h> 
#include <LoRa.h>
#include "DHT.h"

#define DHTPIN 4
//#define DHTTYPE DHT11   // DHT 11
#define DHTTYPE DHT22   // DHT 22  (AM2302), AM2321
//#define DHTTYPE DHT21   // DHT 21 (AM2301)

DHT dht(DHTPIN, DHTTYPE);

String outgoing;
byte msgCount = 0;
byte MasterNode = 0xFF;
byte Node1 = 0xBB;

int velocity(){
  int sensorValue = analogRead(A0);
  float outvoltage = sensorValue * (5.0 / 1023.0);
  int Level = 6 * outvoltage; //The level of wind speed is proportional to the output voltage.
  return Level;
}

String climate(){
  //Serial.println("Reading Value");
  float humidity = dht.readHumidity();
  float temperature = dht.readTemperature();
  String output = "{\"humidity\":none,\"temperature\":none,\"feel_like\":none,\"wind_speed\":" + String(velocity()) +"}";
  if (isnan(humidity) || isnan(temperature)) {
    /*Serial.println(F("Failed to read from DHT sensor!"));
    return;
    output = "{\"humidity\":none" +
             ",\"temperature\":none" +
             ",\"feel_like\":none" + 
             ",\"wind_speed\":" + String(velocity()) +"}";*/
  }
  else{
    float feel_like = dht.computeHeatIndex(temperature, humidity, false);
  
    output = "{\"humidity\":" + String(humidity) +
             ",\"temperature\":" + String(temperature) +
             ",\"feel_like\":" + String(feel_like) + 
             ",\"wind_speed\":" + String(velocity()) +"}";
  }
  Serial.println(output);
  return output;
}

void setup() {
  Serial.begin(9600);
  //while (!Serial);

  Serial.println("LoRa Duplex");
  //(int ss, int reset, int dio0)
  //LoRa.setPins(8, 4, 7);
  dht.begin();
  LoRa.setPins(10, 9, 2);
  if (!LoRa.begin(433E6)) {
    Serial.println("LoRa init failed. Check your connections.");
    while (true);
  }

  Serial.println("LoRa init succeeded.");
}

void loop() {
  onReceive(LoRa.parsePacket());
}

void sendMessage(String outgoing, byte MasterNode, byte otherNode) {
  LoRa.beginPacket();                   // start packet
  LoRa.write(MasterNode);              // add destination address
  LoRa.write(Node1);             // add sender address
  LoRa.write(msgCount);                 // add message ID
  LoRa.write(outgoing.length());        // add payload length
  LoRa.print(outgoing);                 // add payload
  LoRa.endPacket();                     // finish packet and send it
  msgCount++;                           // increment message ID
}

void onReceive(int packetSize) {
  if (packetSize == 0) return;          // if there's no packet, return

  // read packet header bytes:
  int recipient = LoRa.read();          // recipient address
  byte sender = LoRa.read();            // sender address
  byte incomingMsgId = LoRa.read();     // incoming msg ID
  byte incomingLength = LoRa.read();    // incoming msg length

  String incoming = "";

  while (LoRa.available()) {
    incoming += (char)LoRa.read();
  }

  if (incomingLength != incoming.length()) {   // check length for error
    // Serial.println("error: message length does not match length");
    ;
    return;                             // skip rest of function
  }

  // if the recipient isn't this device or broadcast,
  if (recipient != Node1 && recipient != MasterNode) {
    //Serial.println("This message is not for me.");
    ;
    return;                             // skip rest of function
  }
  Serial.println(incoming);
  int Val = incoming.toInt();
  if (Val == 34) {
    String message = climate();
    sendMessage(message, MasterNode, Node1);
    delay(100);
  }

}
