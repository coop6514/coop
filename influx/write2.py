from connect import bucket, org, client
from influxdb_client.client.write_api import SYNCHRONOUS

write_api = client.write_api(write_options=SYNCHRONOUS)

sequence = ["data,host=host1 temperature=24.26",
            "data,host=host1 humidity=65.32",
            "data,host=host1 feel_like=25.12",
            "data,host=host1 wind=1"]
write_api.write(bucket, org, sequence)