from connect import bucket, org, client
from influxdb_client.client.write_api import SYNCHRONOUS

query = """option v = {timeRangeStart: -1h, timeRangeStop: now()}

from(bucket: "climate") 
    |> range(start: -2h)
    |> filter(fn: (r) => r["_measurement"] == "data")
    |> filter(fn: (r) => r["_field"] == "humidity")
    |> filter(fn: (r) => r["host"] == "host1")"""

tables = client.query_api().query(query, org=org)
for table in tables:
    for record in table.records:
        print(record["_time"],record["_field"],record["_value"])