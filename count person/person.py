from math import sqrt

print("importing")
import cv2
import cvlib as cv
from cvlib.object_detection import draw_bbox
print("import success")
pic = cv2.imread("./demo2.jpg")
print("cap loading")
cap = cv2.VideoCapture("./demo.mp4")
rate = 0.2
count = 0
print("cap load success")
while True:
    ret,pic = cap.read()
    h,w = pic.shape[:2]
    line_x = round(w*rate)
    line_y = round(h*rate)

    bbox, label, conf = cv.detect_common_objects(pic, confidence=0.75)

    for i in range(len(bbox)):
        if label[i] == "person":
            x1 = bbox[i][0]
            y1 = bbox[i][1]
            x2 = bbox[i][2]
            y2 = bbox[i][3]

            width = x2-x1
            height = y2-y1

            center_x = x1 + round(width/2)
            center_y = y1 + round(height/2)

            if center_y == line_y:
                count += 1
                print(count)

            cv2.line(pic, (center_x,center_y), (center_x,center_y), (0, 0, 255), 10)

    # cv2.line(pic, (line_x, 0), (line_x, h), (0, 200, 100), 2)
    cv2.line(pic, (0, line_y), (w, line_y), (0, 200, 100), 4)
    draw_bbox(pic, bbox, label, conf)
    if cv2.waitKey(25) & 0xFF == ord('q'):
        break

    cv2.imshow("demo",pic)

cap.release()
cv2.destroyAllWindows()
