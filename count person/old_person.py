import cv2
import time
import numpy as np
from PIL import Image
import tensorflow as tf
from object_detection.utils import ops as utils_ops
from object_detection.utils import visualization_utils as vis_util

model = tf.saved_model.load("/Users/nontakorn/.keras/datasets/ssd_mobilenet_v1_coco_2017_11_17/saved_model")
model = model.signatures['serving_default']

category_index = {1: {'id': 1, 'name': 'person'}}

def show_inference(model, image):
    input_tensor = tf.convert_to_tensor(image)
    input_tensor = input_tensor[tf.newaxis, ...]

    output_dict = model(input_tensor)

    boxes = np.squeeze(output_dict['detection_boxes'])
    scores = np.squeeze(output_dict['detection_scores'])
    classes = np.squeeze(output_dict['detection_classes'])

    indices = np.argwhere(classes == 1)
    boxes = np.squeeze(boxes[indices])
    scores = np.squeeze(scores[indices])
    classes = np.squeeze(classes[indices])
    classes = classes.astype(np.int64)

    vis_util.visualize_boxes_and_labels_on_image_array(
        image,
        boxes,
        classes,
        scores,
        category_index,
        use_normalized_coordinates=True,
        line_thickness=8)

    return image

img = cv2.imread("demo.jpeg")
img = cv2.cvtColor(img,cv2.COLOR_BGR2RGB)
h,w,s = img.shape
img = cv2.resize(img,(round(w/2),round(h/2)))
start =time.time()
cv2.imshow("demo", cv2.cvtColor(show_inference(model, img),cv2.COLOR_BGR2RGB))
print("time :",time.time()-start)
cv2.waitKey(0)

#print(run_inference_for_single_image(model, "demo.jpeg"))