import cv2
import numpy as np
import tensorflow as tf
import dlib

from object_detection.utils import ops as utils_ops

from trackable_object import TrackableObject
from centroidtracker import CentroidTracker

import cvlib as cv
from cvlib.object_detection import draw_bbox

# patch tf1 into `utils.ops`
utils_ops.tf = tf.compat.v1

# Patch the location of gfile
tf.gfile = tf.io.gfile

def run_inference(cap, roi_position=0.6, x_axis=True, show=True,confs=0.75):
    counter = [0, 0, 0, 0]  # left, right, up, down
    total_frames = 0

    ct = CentroidTracker(maxDisappeared=40, maxDistance=50)
    trackers = []
    trackableObjects = {}

    while cap.isOpened():
        ret, image_np = cap.read()
        if not ret:
            break

        height, width, _ = image_np.shape
        #image_np = cv2.flip(image_np, 1)
        rects = []

        if total_frames % 13 == 0:
            print("*", end =" ")
        #if True:
            trackers = []

            # Actual detection.
            bbox, label, conf = cv.detect_common_objects(image_np, confidence=confs)
            for i, (x_min, y_min, x_max, y_max) in enumerate(bbox):
                if label[i] in ["person","car"]:
                    tracker = dlib.correlation_tracker()
                    rect = dlib.rectangle(int(x_min), int(y_min), int(x_max), int(y_max))
                    tracker.start_track(image_np, rect)
                    trackers.append(tracker)
        else:
            status = "Tracking"
            for tracker in trackers:
                # update the tracker and grab the updated position
                tracker.update(image_np)
                pos = tracker.get_position()

                # unpack the position object
                x_min, y_min, x_max, y_max = int(pos.left()), int(
                    pos.top()), int(pos.right()), int(pos.bottom())

                # add the bounding box coordinates to the rectangles list
                rects.append((x_min, y_min, x_max, y_max))

        objects = ct.update(rects)

        for (objectID, centroid) in objects.items():
            to = trackableObjects.get(objectID, None)

            if to is None:
                to = TrackableObject(objectID, centroid)
            else:
                if x_axis and not to.counted:
                    x = [c[0] for c in to.centroids]
                    direction = centroid[0] - np.mean(x)

                    if centroid[0] > roi_position*width and direction > 0 and np.mean(x) < roi_position*width:
                        counter[1] += 1
                        to.counted = True
                    elif centroid[0] < roi_position*width and direction < 0 and np.mean(x) > roi_position*width:
                        counter[0] += 1
                        to.counted = True

                elif not x_axis and not to.counted:
                    y = [c[1] for c in to.centroids]
                    direction = centroid[1] - np.mean(y)

                    if centroid[1] > roi_position*height and direction > 0 and np.mean(y) < roi_position*height:
                        counter[3] += 1
                        to.counted = True
                    elif centroid[1] < roi_position*height and direction < 0 and np.mean(y) > roi_position*height:
                        counter[2] += 1
                        to.counted = True

                to.centroids.append(centroid)

            trackableObjects[objectID] = to

            text = "ID {}".format(objectID)
            cv2.putText(image_np, text, (centroid[0] - 10, centroid[1] - 10),
                        cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 2)
            cv2.circle(
                image_np, (centroid[0], centroid[1]), 4, (255, 255, 255), -1)

        # Draw ROI line
        if x_axis:
            cv2.line(image_np, (int(roi_position*width), 0),
                     (int(roi_position*width), height), (0xFF, 0, 0), 5)
        else:
            cv2.line(image_np, (0, int(roi_position*height)),
                     (width, int(roi_position*height)), (0xFF, 0, 0), 5)

        # display count and status
        font = cv2.FONT_HERSHEY_SIMPLEX
        if x_axis:
            cv2.putText(image_np, f'Left: {counter[0]}; Right: {counter[1]}', (10, 35), font, 0.8, (191, 0, 0), 2, cv2.FONT_HERSHEY_SIMPLEX)
        else:
            cv2.putText(image_np, f'Up: {counter[2]}; Down: {counter[3]}', (10, 35), font, 0.8, (191, 0, 0), 2, cv2.FONT_HERSHEY_SIMPLEX)
        #cv2.putText(image_np, 'Status: ' + status, (10, 70), font, 0.8, (0, 0xFF, 0xFF), 2, cv2.FONT_HERSHEY_SIMPLEX)

        if show:
            #draw_bbox(image_np, bbox, label, conf)
            #cv2.imshow('cumulative_object_counting', image_np)
            if cv2.waitKey(25) & 0xFF == ord('q'):
                break

        total_frames += 1

    cap.release()
    cv2.destroyAllWindows()
    print(counter)


if __name__ == '__main__':
    #cap = cv2.VideoCapture("rtsp://admin:88888888@172.16.40.97:10554/tcp/av0_1")
    #cap = cv2.VideoCapture(0)
    cap = cv2.VideoCapture("./demo1.mp4")

    if not cap.isOpened():
        print("Error opening video stream or file")

    for i in range(20):
        print("conf",i/20,": ")
        run_inference(cap,
                    roi_position=0.3,
                    x_axis=True,
                    show=True,
                    confs=i/20)

